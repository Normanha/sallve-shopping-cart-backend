import { prop, getModelForClass } from "@typegoose/typegoose";

export class Product {
  @prop({ required: true })
  public name?: string;

  @prop({ required: true })
  public image?: string;
}

export const ProductModel = getModelForClass(Product);
