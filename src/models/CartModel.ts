import { prop, getModelForClass, modelOptions } from "@typegoose/typegoose";

export class CartProduct {
  @prop({ required: true })
  public sku?: string;

  @prop({ required: true })
  public name?: string;

  @prop({ required: true })
  public image?: string;

  @prop({ required: true })
  public price?: number;

  @prop({ required: true })
  public quantity?: number;

  @prop({ required: true })
  public subTotal?: number;
}

@modelOptions({
  schemaOptions: {
    versionKey: false,
  },
})
export class Cart {
  @prop({ required: true })
  public products?: CartProduct[] | [];

  @prop({ required: true })
  public totalPrice?: number;

  @prop({ required: true })
  public totalQuantity?: number;
}

export const CartModel = getModelForClass(Cart);
