import { prop, getModelForClass, Ref } from "@typegoose/typegoose";
import { Product } from "./ProductModel";

export class Sku {
  @prop({ required: true, unique: true })
  public sku?: string;

  @prop({ required: true })
  public inventory?: number;

  @prop({ required: true })
  public price?: number;

  @prop({ required: true, ref: () => Product })
  public product?: Ref<Product>;
}

export const SkuModel = getModelForClass(Sku);
