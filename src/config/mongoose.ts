import { ConnectionOptions } from "mongoose";

export const serverUrl: string = "mongodb://localhost:27017/shopping-cart";

export const connectionOptions: ConnectionOptions = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
