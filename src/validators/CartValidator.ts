import { param, validationResult } from "express-validator";

import { Request, Response, NextFunction } from "express";
import { createCart } from "../controllers/CartController";

export const getCartValidationRules = () => {
  return [param("cartId").isMongoId()];
};

export const validateGetCart = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  //If cart does not exist or has been deleted, we create a new cart
  if (errors.mapped().cartId) {
    return createCart(req, res);
  }

  return res.status(422).json(errors.mapped());
};
