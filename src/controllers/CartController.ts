import { Request, Response } from "express";
import { Cart, CartModel, CartProduct } from "../models/CartModel";
import { SkuModel, Sku } from "../models/SkuModel";
import { ProductModel } from "../models/ProductModel";

export async function createCart(req: Request, res: Response) {
  try {
    const { products, totalPrice, totalQuantity } = req.body;

    let createdCart: Cart = {};

    if (products && totalPrice && totalQuantity) {
      createdCart = await CartModel.create({
        products,
        totalPrice,
        totalQuantity,
      } as Cart);
    } else {
      createdCart = await CartModel.create({
        products: [],
        totalPrice: 0,
        totalQuantity: 0,
      } as Cart);
    }
    return res.status(201).json({ code: 201, status: "ok", data: createdCart });
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}

export async function getCart(req: Request, res: Response) {
  try {
    const { cartId } = req.params;
    const cart = await CartModel.findOne({ _id: cartId });

    if (cart) {
      return res.status(200).json({ code: 200, status: "ok", data: cart });
    } else {
      return createCart(req, res);
    }
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}

export async function addProductToCart(req: Request, res: Response) {
  try {
    const { cartId } = req.params;
    const { sku, quantity } = req.body;

    if (cartId) {
      const cart = await CartModel.findOne({ _id: cartId });

      if (cart) {
        const skuDocument = await SkuModel.findOne({ sku: sku });

        const productDocument = await ProductModel.findOne({
          _id: skuDocument?.product,
        });

        if (!skuDocument && !productDocument) {
          return res
            .status(404)
            .json({ code: 404, status: "error", message: "Product not found" });
        }

        if (skuDocument.inventory === 0 || quantity > skuDocument.inventory) {
          return res.status(422).json({
            code: 422,
            status: "error",
            message: "Product has not enough inventory",
          });
        }

        let newCartProducts: CartProduct[];
        let productIndex = -1;

        const availableProduct = cart.products?.find((product, index) => {
          if (product.sku === sku) {
            productIndex = index;
            return product;
          }
        });

        if (availableProduct) {
          const newQuantity = availableProduct.quantity + quantity;

          const newProduct: CartProduct = {
            ...availableProduct,
            quantity: newQuantity,
            subTotal: availableProduct.price * newQuantity,
          };

          newCartProducts = [...cart.products];

          newCartProducts.splice(productIndex, 1, newProduct);
        } else {
          const newProduct: CartProduct = {
            sku,
            name: productDocument?.name,
            image: productDocument?.image,
            price: skuDocument?.price,
            quantity,
            subTotal: quantity * skuDocument?.price,
          };

          newCartProducts = [...cart.products, newProduct];
        }

        await SkuModel.updateOne({ sku: sku }, {
          inventory: skuDocument.inventory - quantity,
        } as Sku);

        const totalQuantity: number | undefined = newCartProducts
          .map((product) => product.quantity)
          .reduce((prevValue, currentValue) => {
            if (prevValue && currentValue) {
              return prevValue + currentValue;
            }
          });

        const totalPrice: number | undefined = newCartProducts
          .map((product) => product.subTotal)
          .reduce((prevValue, currentValue) => {
            if (prevValue && currentValue) {
              return prevValue + currentValue;
            }
          });

        const newCart = {
          products: newCartProducts,
          totalPrice,
          totalQuantity,
        };

        await CartModel.updateOne({ _id: cartId }, newCart);

        return res.status(201).json({ code: 201, status: "ok", data: newCart });
      } else {
        return res
          .status(404)
          .json({ code: 404, status: "error", message: "Cart not found" });
      }
    } else {
      return res
        .status(404)
        .json({ code: 404, status: "error", message: "Cart not found" });
    }
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}

export async function updateProductInCart(req: Request, res: Response) {
  try {
    const { cartId } = req.params;
    const { sku, quantity } = req.body;

    if (cartId) {
      const cart = await CartModel.findOne({ _id: cartId });
      if (cart) {
        const skuDocument = await SkuModel.findOne({ sku: sku });
        const productDocument = await ProductModel.findOne({
          _id: skuDocument?.product,
        });

        if (!skuDocument && !productDocument) {
          return res
            .status(404)
            .json({ code: 404, status: "error", message: "Product not found" });
        }

        let newCartProducts: CartProduct[];
        let productIndex = -1;

        const availableProduct = cart.products?.find((product, index) => {
          if (product.sku === sku) {
            productIndex = index;
            return product;
          }
        });

        if (availableProduct) {
          let newInventoryQuantity = skuDocument.inventory;

          if (availableProduct.quantity > quantity) {
            newInventoryQuantity =
              skuDocument.inventory + (availableProduct.quantity - quantity);
          } else {
            newInventoryQuantity =
              skuDocument.inventory - (quantity - availableProduct.quantity);
          }

          if (newInventoryQuantity < 0) {
            return res.status(422).json({
              code: 422,
              status: "error",
              message: "Product has not enough inventory",
            });
          }

          await SkuModel.updateOne({ sku: sku }, {
            inventory: newInventoryQuantity,
          } as Sku);

          const newProduct: CartProduct = {
            ...availableProduct,
            quantity: quantity,
            subTotal: availableProduct.price * quantity,
          };

          newCartProducts = [...cart.products];

          newCartProducts.splice(productIndex, 1, newProduct);
        } else {
          if (skuDocument.inventory === 0 || quantity > skuDocument.inventory) {
            return res.status(422).json({
              code: 422,
              status: "error",
              message: "Product has not enough inventory",
            });
          }

          await SkuModel.updateOne({ sku: sku }, {
            inventory: skuDocument.inventory - quantity,
          } as Sku);

          const newProduct: CartProduct = {
            sku,
            name: productDocument?.name,
            image: productDocument?.image,
            price: skuDocument?.price,
            quantity,
            subTotal: quantity * skuDocument?.price,
          };

          newCartProducts = [...cart.products, newProduct];
        }

        const totalQuantity: number | undefined = newCartProducts
          .map((product) => product.quantity)
          .reduce((prevValue, currentValue) => {
            if (prevValue && currentValue) {
              return prevValue + currentValue;
            }
          });

        const totalPrice: number | undefined = newCartProducts
          .map((product) => product.subTotal)
          .reduce((prevValue, currentValue) => {
            if (prevValue && currentValue) {
              return prevValue + currentValue;
            }
          });

        const newCart = {
          products: newCartProducts,
          totalPrice,
          totalQuantity,
        };

        await CartModel.updateOne({ _id: cartId }, newCart);

        return res.status(200).json({ code: 200, status: "ok", data: newCart });
      } else {
        return res
          .status(404)
          .json({ code: 404, status: "error", message: "Cart not found" });
      }
    } else {
      return res
        .status(404)
        .json({ code: 404, status: "error", message: "Cart not found" });
    }
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}

export async function removeProductFromCart(req: Request, res: Response) {
  try {
    const { cartId, sku } = req.params;
    if (cartId) {
      const cart = await CartModel.findOne({ _id: cartId });
      if (cart) {
        const skuDocument = await SkuModel.findOne({ sku: sku });
        let newCartProducts: CartProduct[];
        let productIndex = -1;
        const availableProduct = cart.products?.find((product, index) => {
          if (product.sku === sku) {
            productIndex = index;
            return product;
          }
        });

        if (availableProduct) {
          await SkuModel.updateOne({ sku: sku }, {
            inventory: availableProduct.quantity + skuDocument.inventory,
          } as Sku);

          newCartProducts = [...cart.products];

          newCartProducts.splice(productIndex, 1);
        } else {
          return res.status(404).json({
            code: 404,
            status: "error",
            message: "Product does not exist on cart",
          });
        }

        let totalQuantity: number = 0;
        let totalPrice: number = 0;

        if (newCartProducts.length > 0) {
          totalQuantity = newCartProducts
            .map((product) => product.quantity)
            .reduce((prevValue, currentValue) => {
              if (prevValue && currentValue) {
                return prevValue + currentValue;
              }
            });

          totalPrice = newCartProducts
            .map((product) => product.subTotal)
            .reduce((prevValue, currentValue) => {
              if (prevValue && currentValue) {
                return prevValue + currentValue;
              }
            });
        }

        const newCart = {
          products: newCartProducts,
          totalPrice,
          totalQuantity,
        };

        await CartModel.updateOne({ _id: cartId }, newCart);

        return res.status(204).json({ code: 204, status: "ok", data: newCart });
      } else {
        return res
          .status(404)
          .json({ code: 404, status: "error", message: "Cart not found" });
      }
    } else {
      return res
        .status(404)
        .json({ code: 404, status: "error", message: "Cart not found" });
    }
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}
