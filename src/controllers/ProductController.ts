import { Request, Response } from "express";
import { SkuModel, Sku } from "../models/SkuModel";
import { ProductModel, Product } from "../models/ProductModel";

export async function getProducts(req: Request, res: Response) {
  try {
    const skus = await SkuModel.find();
    if (skus) {
      const skusWithProductData = await Promise.all(
        skus.map(async (skuItem: Sku) => {
          const relatedProduct = await ProductModel.findOne({
            _id: skuItem.product,
          });

          return {
            sku: skuItem.sku,
            name: relatedProduct.name,
            image: relatedProduct.image,
            price: skuItem.price,
          };
        })
      );
      return res
        .status(200)
        .json({ code: 200, status: "ok", data: skusWithProductData });
    } else {
      return res.status(200).json({ code: 200, status: "ok", data: [] });
    }
  } catch (err) {
    return res
      .status(500)
      .json({ code: 500, status: "error", message: "Internal server error" });
  }
}
