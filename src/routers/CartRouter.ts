import express, { Request, Response } from "express";
import {
  addProductToCart,
  createCart,
  getCart,
  removeProductFromCart,
  updateProductInCart,
} from "../controllers/CartController";
import {
  getCartValidationRules,
  validateGetCart,
} from "../validators/CartValidator";

const router = express.Router();

//Create the Cart
router.post("/v1/carts", async (req: Request, res: Response) => {
  return createCart(req, res);
});

//Fetch the cart
router.get(
  "/v1/carts/:cartId",
  getCartValidationRules(),
  validateGetCart,
  async (req: Request, res: Response) => {
    return getCart(req, res);
  }
);

//Add a product to cart
router.post("/v1/carts/:cartId/", async (req: Request, res: Response) => {
  return addProductToCart(req, res);
});

//Update a product in cart
router.put("/v1/carts/:cartId/", async (req: Request, res: Response) => {
  return updateProductInCart(req, res);
});

//Remove a product from cart
router.delete("/v1/carts/:cartId/:sku", async (req: Request, res: Response) => {
  return removeProductFromCart(req, res);
});

export default router;
