import express, { Request, Response } from "express";
import { getProducts } from "../controllers/ProductController";

const router = express.Router();

router.get("/v1/products", async (req: Request, res: Response) => {
  return getProducts(req, res);
});

export default router;
