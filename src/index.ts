import express from "express";
import mongoose from "mongoose";
import { json } from "body-parser";
import { serverUrl, connectionOptions } from "./config/mongoose";
import { CartRouter, ProductRouter } from "./routers";

const app = express();

app.use(json());

app.use("/api", [CartRouter, ProductRouter]);

mongoose.connect(serverUrl, connectionOptions, () => {
  console.log("Connected to database");
});

app.listen(3000, () => {
  console.log("Sallve Shopping Cart Server connected in Port 3000.");
});
